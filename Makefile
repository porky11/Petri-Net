.RECIPEPREFIX+= 

FLAGS=-M../dale-extensions

all: test

test: libpetri-net.dtm src/test.dt
  dalec src/test.dt -o test $(FLAGS)

libpetri-net.dtm: src/petri-net.dt
  dalec -c src/petri-net.dt $(FLAGS)

src/%.dt:
  true

.PHONY: all clean

clean:
  rm -f test && rm *.so && rm *.bc && rm *.dtm

